import requests
import random
import time

resources = ['pepe.jpg', 'style.css', 'index.js']

uas = ['curl/7.58.0', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.3 Safari/534.24', 'Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/60.0.3112.107 Mobile Safari/537.36', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1']

for i in range(1000):
    resource = 'http://localhost:8080/'+random.choice(resources)
    headers = {'User-Agent':random.choice(uas)}
    r = requests.get(resource, headers=headers)
    time.sleep(random.random()*5)

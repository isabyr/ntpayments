# Introduction


# Requirements
 - Docker, docker-compose
 - Python 2
 - `sudo sysctl -w vm.max_map_count=262144` elastic search requires

# Docker services structure
![Image of Yaktocat](https://docs.google.com/drawings/d/e/2PACX-1vR6h5eXaKH9b9aRcsg8KkB6DWERV7PNog3UrB6GrqnlZdtCUhhQT2j4fRkXq6eb5kOAn_1dkRYuOZen/pub?w=960&h=720)

- Nginx-proxy intercepts all requests to web server(nginx-web) and logs to Fluentd
- Fluentd transforms logs and streams to ElasticSearch
- Kibana visualizes all the data

# How to run

- `sudo docker-compose up`
- `python main.py` - generates random requests to files with various user-agents
- `http://localhost:5601` - Kibana dashboard
- `http://localhost:8080` - web site
